# Valhallaland!
A game by Muse Systems as designed by Steven C. Buttgereit

## Project Purpose
Valhallaland is being developed less as an actual game to be played, but more 
as a toy project while learning the Rust programming language.

Do understand that this code will not be clean, very "Rustic", and it may 
never reach a playable state.  I'm doing this to make many of my early 
mistakes with Rust and to start forming opinions about how best to use the 
tool.

## Copyright
All original code developed by Muse Systems or contributors in Valhallaland! 
is Copyright 2015 Lima Buttgereit Holdings LLC d/b/a Muse Systems.  Third 
party libraries and incorporated code remain the intellectual property of 
their respective copyright holders and their license terms will be included 
in this repository in an appropriate and reasonable location. 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License Version 2 as published 
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
